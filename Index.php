<?php
    include 'koneksi.php';
?>
<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta nama="viewport" content="width=device-width, initial-scale=1.0 ">
       <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Aplikasi CRUD</title>
</head>
<div class="container mt-3">
<body>
<h1 class="text-center">TABLE MAHASISWA</h1>
 <div class="container mt-3">
     <a href="add.php" class="btn btn-primary btn-md mb-3">Tambah Data</a>
    <table class="table table-striped table-hover table-bordered ">
    <thead class="table-dark">
        <th>NPM</th>
        <th>Nama Mahasiswa</th>
        <th>Jurusan</th>
        <th>Alamat </th>
        <th>Telepon</th>
        <th>Aksi</th>
</thead>

<?php
$sqlGet = "SELECT * FROM mahasiswa";
$query = mysqli_query($conn, $sqlGet);

while($data = mysqli_fetch_array($query)) {
echo "
<tbody>
    <tr>
        <td>$data[npm]</td>
        <td>$data[nama]</td>
        <td>$data[jurusan]</td>
        <td>$data[alamat]</td>
        <td>$data[telp]</td>
        <td>
        <div class='row'>
        <div class='col d=flex justify=content=center'>
    <a href='update.php?npm=$data[npm]' class='btn btn-sm btn-warning'>Update</a>
    </div>
    <div class='col d=flex justify=content=center'>
    <a href='delete.php?npm=$data[npm]' class='btn btn-sm btn-danger'>Delete</a>
        </div>
    </div>

            </td>

    </tr>
    </tbody>

";
}
?>

</table>
</div>
   
        </body>
</html>