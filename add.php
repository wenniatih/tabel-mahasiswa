<?php
    include 'koneksi.php';
?>
<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta nama="viewport" content="width=device-width, initial-scale=1.0 ">
       <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Aplikasi CRUD</title>
</head>
<body>
    <div class="w-50 mx-auto border p-3 mt-3">
    <a href="index.php">Kembali ke home</a>
    <form action="add.php" method="post">
        <label for="npm">NPM</label>
        <input type="text" id="npm" name="npm" class="form-control" required>

        <label for="nama">Nama Mahasiswa</label>
        <input type="text" id="nama" name="nama" class="form-control" required >

        <label for="jurusan">Jurusan</label>
       <select name="jurusan" id="jurusan" class="form-select" required>
    <option >Pilih Jurusan</option>
       <option value="informatika">Teknik informatika</option>
       <option value="informasi">Sistem informasi</option>
       <option value="komputer">Teknik Komputer</option>
    </select>

        <label for="alamat">Alamat</label>
        <input type="text" id="alamat" name="alamat" class="form-control" requried>

        <label for="telp">Telepon</label>
        <input type="text" id="telp" name="telp" class="form-control" required>

        <input class="btn btn-success mt-3" type="submit" name="tambah" value="Tambah Data">

    </form>
</div>

<?php

if(isset($_POST['tambah'])){
    $npm = $_POST['npm'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $telp = $_POST['telp'];
    
   $jurusanSelect = $_POST['jurusan'];
   if($jurusanSelect == 'informatika') {
        $jurusan = 'Teknik Informatika';
   } if($jurusanSelect == 'informasi') {
        $jurusan = 'Sistem Informasi';
   }if($jurusanSelect == 'komputer') {
    $jurusan = 'Teknik Komputer';
   }

    $sqlGet = "SELECT * FROM mahasiswa WHERE npm='$npm'";
    $queryGet = mysqli_query($conn, $sqlGet);
    $cek = mysqli_num_rows($queryGet);


    $sqlInsert = "INSERT INTO mahasiswa(npm,nama,jurusan,alamat,telp)
                VALUES ('$npm','$nama','$jurusan','$alamat','$telp')";
     
    $queryInsert = mysqli_query($conn, $sqlInsert);    

     if(isset($sqlInsert) && $cek <= 0 ) {
        echo "
        <div class='alert alert-success'>Data berhasil ditambahkan <a href='index.php'>Lihat data</a></div>
        ";
    } else if ($cek > 0  ) {
    echo"
       <div class='alert alert-danger'>Data gagal ditambahkan <a href='index.php'>Lihat data</a></div>
    ";
    }

}
?>

        </body>
</html>